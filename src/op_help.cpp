#include "cmdline.hpp"
#include "config.h"
#include "safmat.hpp"

namespace oips::cmdline {
	using safmat::println;
	using safmat::print;
	struct help_operation : operation {
		help_operation()
			: operation{ "help", " [operation]", "Prints help.", {} } {}

		int operator()(const args_list &args) override;
	};

	static help_operation op_help;
	operation *operations::help = &op_help;

	int help_operation::operator()(const args_list &args) {
		static constexpr unsigned help_len = 40;
		if (args.size() > 1) {
			println(stderr, "Usage: oips help [operation]");
			return 1;
		}

		const auto print_header = [] {
			println("OverlayFS Image Packaging System {}", PACKAGE_VERSION);
			println("");
			println("Usage:");
		};

		const auto print_opt = [](const option &opt) {
			bool first = true;
			for (const auto name : opt.names) {
				if (opt.takes_arg) {
					print("  {} <ARG>{:{}}", name, "", help_len - name.length() - 6);
				} else {
					print("  {:{}}", name, help_len);
				}

				if (first) {
					println("{}", opt.description);
					first = false;
				} else {
					println("Alias for {}", opt.names[0]);
				}
			}
		};


		operation *op2;
		if (args.size() == 0) {
			print_header();
			println("  oips [...] <operation> [...]");
			println("");
			println("Operations:");
			for (const auto *op : operation::operations) {
				println("  {}{}{:{}}{}", op->name, op->usage, "",
						help_len - op->name.length() - op->usage.length(),
						op->description);
			}
			println("");
			println("Global options:");
			op2 = operation::global;
		} else {
			op2 = operation::get_op(args[0]);
			if (!op2) {
				println(stderr, "oips: Invalid operation: {}", args[0]);
				return 1;
			}

			print_header();
			println("  oips {}{}", op2->name, op2->usage);
			println("");
			println("Description:");
			println("  {}", op2->description);
			if (!op2->options.empty()) {
				println("\nOptions:");
			}
		}
		for (const auto &opt : op2->options)
			print_opt(opt);
		return 0;
	}
} // namespace oips::cmdline
