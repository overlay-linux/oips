#include <cstdlib>
#include "download.hpp"
#include "safmat.hpp"

namespace oips {
	static bool download_wget(std::string_view url, std::string_view filename) {
		const auto cmdline = safmat::format("wget -o '{}' '{}'", filename, url);
		const auto result = std::system(cmdline.c_str());
		return result == 0;
	}
	bool download(std::string_view url, std::string_view filename) {
		return download_wget(url, filename);
	}
}
