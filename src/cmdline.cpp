#include "cmdline.hpp"
#include "error.hpp"
#include "safmat.hpp"
#include <algorithm>
#include <source_location>
#include <stdexcept>

namespace oips::cmdline {
	using safmat::println;
	struct global_operation : operation {
		global_operation()
			: operation{ "<global>",
						 "no usage",
						 "global operation",
						 {
							 { { "-h", "--help" }, "Print a help page", false },
						 } } {}
		int operator()(const args_list &args) override {
			if ((*this)["-h"].selected) {
				return (*operations::help)(args);
			}
			println(stderr, "Usage: oips help");
			return 1;
		}
	};
	static global_operation global_op;
	operation *operation::global = &global_op;

	option *operation::get(std::string_view name) {
		for (auto &opt : options) {
			for (auto n : opt.names) {
				if (n == name)
					return &opt;
			}
		}
		return nullptr;
	}
	option &operation::operator[](std::string_view name) {
		if (auto opt = get(name)) {
			return *opt;
		} else {
			raise<std::invalid_argument>(
				safmat::format("Invalid option '{}'", name));
		}
	}

	operation *operation::get_op(std::string_view name) {
		for (auto op : operation::operations) {
			if (op->name == name)
				return op;
		}
		return nullptr;
	}

	int parse(int argc, char *argv[]) {
		args_list args{};
		operation *op = operation::global;
		bool parse_opts = true;
		int idx = 1;

		for (; idx < argc && parse_opts; ++idx) {
			std::string_view arg = argv[idx];

			if (arg == "-") {
				break;
			} else if (arg == "--") {
				parse_opts = false;
				++idx;
				break;
			} else if (!arg.empty() && arg[0] == '-') {
				std::string_view optname;
				if (arg[1] == '-') {
					const auto eq = std::find(begin(arg), end(arg), '=');
					optname = arg.substr(0, eq - optname.begin());
				} else {
					optname = arg.substr(0, 2);
				}

				auto opt = op->get(optname);
				if (!opt) {
					println(stderr, "oips: Invalid option: {}", optname);
					return 1;
				}

				opt->selected = true;

				if (opt->takes_arg) {
					if (optname == arg) {
						if (idx == argc) {
							println(stderr,
									"oips: Option '{}' expects an argument.");
							return 1;
						}
						opt->arg = argv[idx++];
					} else {
						opt->arg = arg.substr(optname.size());
					}
				} else if (optname != arg) {
					println(stderr,
							"oips: Option '{}' doesn't expect an argument.");
					return 1;
				}
			} else {
				if (op == operation::global) {
					op = operation::get_op(arg);
					if (op == nullptr) {
						println(stderr, "oips: Invalid operation: {}", arg);
						return 1;
					}
				} else {
					args.push_back(arg);
				}
			}
		}

		return (*op)(args);
	}
} // namespace oips::cmdline
