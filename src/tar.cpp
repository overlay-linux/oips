#include <safmat.hpp>
#include <unistd.h>
#include <cstring>
#include <cstdio>
#include <cerrno>
#include "utils.hpp"
#include "tar.hpp"

namespace oips {
	result<tar> tar::open(std::string filename) {
		if (::access(filename.c_str(), R_OK))
			return error("Failed to open '{}': File is not readable.", filename);

		return ok(tar(std::move(filename)));
	}

	result<std::vector<std::string>> tar::list() const {
		const auto cmdline = safmat::format("tar -tf '{}'", filename);
		std::FILE *input = ::popen(cmdline.c_str(), "r");
		if (!input) {
			return error("Failed to run '{}': {}.", cmdline, std::strerror(errno));
		}

		auto files = read_lines(input);

		if (const int ec = ::pclose(input); ec != 0)
			return error("tar exited with code {}.", ec);
		
		return ok(std::move(files));
	}

	result<std::string> tar::read_file(std::string_view name) const {
		const auto cmdline = safmat::format("tar -xf '{}' -O '{}'", filename, name);
		std::FILE *input = ::popen(cmdline.c_str(), "r");
		if (!input) {
			return error("Failed to run '{}': {}", cmdline, std::strerror(errno));
		}

		auto contents = read(input);

		if (const int ec = ::pclose(input); ec != 0)
			return error("tar exited with code {}.", ec);
		
		return ok(std::move(contents));
	}
	result<std::monostate> tar::extract_to(std::string_view dir) const {
		const auto cmdline = safmat::format("tar -C '{}' -xf '{}'", dir, filename);

		if (const int ec = std::system(cmdline.c_str()); ec != 0) {
			return error("tar exited with code {}.", ec);
		} else {
			return ok();
		}
	}
}
