#include <cctype>
#include <cstdio>
#include "safmat.hpp"
#include "conf.hpp"


namespace oips::conf {
	result<config> read_file(const std::string &filename) {
		std::string str{};
		std::FILE *file = std::fopen(filename.c_str(), "r");
		char buffer[100];

		while (std::fgets(buffer, sizeof buffer, file) != nullptr) {
			str += buffer;
		}

		return read_string(str);
	}
	result<config> read_string(std::string_view str) {
		std::size_t line = 0;
		std::map<std::string, std::string> conf{};
		//std::string section{};

		auto peek = [&] {
			return str.empty() ? EOF : static_cast<int>(str[0]);
		};
		auto next = [&] {
			if (str.empty()) {
				return EOF;
			} else {
				const int ch = str[0];
				str = str.substr(1);
				return ch;
			}
		};
		auto isname = [](int ch) { return std::isalnum(ch) || ch == '-'; };

		auto skip_ws = [&] {
			while (std::isspace(peek()) && peek() != '\n')
				next();
		};

		while (!str.empty()) {
			++line;

			while (std::isspace(peek())) {
				if (next() == '\n')
					continue;
			}

			if (peek() == EOF)
				break;

			if (peek() == '#') {
				while (peek() != '\n')
					next();
				next();
				continue;
			}
			
			/*if (peek() == '[') {
				section.clear();
				next();

				skip_ws();

				while (isname(peek()))
					section += next();

				skip_ws();

				if (peek() != ']')
					return safmat::format("{}: Invalid input: '{}'", line, peek());

				next();

				skip_ws();

				if (peek() != '\n')
					return safmat::format("{}: Garbage at the end of line", line);
				continue;
			}*/

			if (isname(peek())) {
				std::string name{}, value{};
				while (isname(peek()))
					name += next();

				skip_ws();
				if (peek() != '=') {
					return error("{}: Expected '=' after '{}'", line, name);
				}
				next();

				while (peek() != '\n')
					value += next();

				// Trim end of string.
				value.erase(value.find_last_not_of(" \t\r\f\v") + 1);

				conf[name] = value;
				continue;
			}

			return error("{}: Garbage at beginning of line: '{}'", line, peek());
		}

		return ok(config(std::move(conf)));
	}
}
