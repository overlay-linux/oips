#include "cmdline.hpp"

int main(int argc, char *argv[]) {
	return oips::cmdline::parse(argc, argv);
}
