#include "conf.hpp"
#include "safmat.hpp"
#include "package.hpp"
#include "tar.hpp"

namespace oips {
	result<package_info> package_info::parse(const conf::config &conf) {
		for (const auto field : { "Name", "Version", "Description", "License", "SHA256", "Target" }) {
			if (conf[field].empty())
				return error("Can't read package.info because field '{}' is empty.", field);
		}

		const auto make_vector = [](std::string_view str) {
			std::vector<std::string> vec{};
			while (!str.empty()) {
				const auto comma = str.find(',');
				if (comma == std::string_view::npos) {
					vec.emplace_back(begin(str), end(str));
					break;
				}
				vec.emplace_back(begin(str), begin(str) + comma);
				str = str.substr(comma);
			}
			return vec;
		};

		package_info info;
		info.name			= conf["Name"];
		info.version		= conf["Version"];
		info.description	= conf["Description"];
		info.url			= conf["URL"];
		info.target			= conf["Target"];
		info.sha256			= conf["SHA256"];
		info.licenses		= make_vector(conf["Licenses"]);
		info.depends		= make_vector(conf["Depends"]);
		info.provides		= make_vector(conf["Provides"]);
		info.replaces		= make_vector(conf["Replaces"]);
		info.conflicts		= make_vector(conf["Conflicts"]);
		return ok(info);
	}

	result<package_info> package_info::extract_from_oip(std::string oip) {
		auto r1 = tar::open(std::move(oip));
		if (!r1)
			return error(r1.get_error());
		auto &archive = r1.unwrap();

		auto r2 = archive.read_file("package.info");
		if (!r2)
			return error("Failed to read package.info: {}", r2.get_error());
		auto &contents = r2.unwrap();

		auto r3 = conf::read_string(contents);
		if (!r3)
			return error("Failed to parse package.info: {}", r3.get_error());

		return package_info::parse(r3.unwrap());
	}
}

void safmat::Formatter<oips::package_info>::format_to(
	FormatContext &ctx, const oips::package_info &info) {
	auto &out = ctx.out;

#define print(name, value) safmat::format_to(out, "{:{}}: {}\n", name, 20, value)
	print("Name", 			info.name);
	print("Version",		info.version);
	print("Description",	info.description);
	print("URL",			info.url);
	print("Target",			info.target);
	print("SHA256",			info.sha256);
	print("License",		info.licenses);
	print("Depends",		info.depends);
	print("Provides", 		info.provides);
	print("Replaces", 		info.replaces);
	print("Conflicts", 		info.conflicts);
}
