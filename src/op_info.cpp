#include <regex>
#include "cmdline.hpp"
#include "package.hpp"
#include "safmat.hpp"

namespace oips::cmdline {
	using safmat::println;

	struct info_operation : operation {
		info_operation()
			: operation{ "info", " <package>", "Show information about a package.",
						 {
						 }} {}

		int operator()(const args_list &args) override;
	};

	static info_operation op_info;
	operation *operations::info = &op_info;

	int info_operation::operator()(const args_list &args) {
		if (args.size() != 1) {
			println(stderr, "Usage: oips info <package>");
			return 1;
		}

		const auto name = args[0];
		if (name.ends_with(".oip")) {
			auto result = package_info::extract_from_oip(std::string(begin(name), end(name)));
			if (result) {
				safmat::print("{}", result.unwrap());
				return 0;
			} else {
				safmat::println(stderr, "oips: {}", result.get_error());
				return 1;
			}
		} else {
			// TODO: Support for downloading packages.
			println(stderr, "Downloading packages is not yet supported.");
			return 1;
		}


		return 0;
	}
}
