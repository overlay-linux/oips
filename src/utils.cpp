#include "utils.hpp"

namespace oips {
	std::string read(std::FILE *file) {
		std::string contents;
		char buffer[100];

		while (std::fgets(buffer, sizeof buffer, file) != nullptr) {
			contents += buffer;
		}

		return contents;
	}
	bool read_line(std::FILE *file, std::string &str) {
		char buffer[100];

		while (true) {
			if (!std::fgets(buffer, sizeof buffer, file))
				return !str.empty();
			const auto len = std::strlen(buffer);
			if (buffer[len - 1] == '\n') {
				buffer[len - 1] = '\0';
				str += buffer;
				return true;
			}
			str += buffer;
		}
	}
	std::vector<std::string> read_lines(std::FILE *file) {
		std::vector<std::string> lines;
		std::string line;

		while (read_line(file, line)) {
			lines.push_back(std::move(line));
			line.clear();
		}

		return lines;
	}
}
