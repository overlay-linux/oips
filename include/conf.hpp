#ifndef FILE_OIPS_CONF_HPP
#define FILE_OIPS_CONF_HPP
#include <string_view>
#include <string>
#include <map>
#include "error.hpp"

namespace oips::conf {
	struct config {
		std::map<std::string, std::string> map;

		config(std::map<std::string, std::string> &&map) : map(std::move(map)) {}
		config(const config &) = default;
		config(config &&) = default;
		~config() = default;

		std::string_view operator[](std::string_view name) const {
			for (const auto &[n, v] : map) {
				if (n == name)
					return v;
			}
			return {};
		}
	};
	
	
	result<config> read_file(const std::string &filename);
	result<config> read_string(std::string_view str);
}

#endif // FILE_OIPS_CONF_HPP
