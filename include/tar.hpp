#ifndef FILE_OIPS_TAR_HPP
#define FILE_OIPS_TAR_HPP
#include <string_view>
#include <variant>
#include <string>
#include <vector>
#include "error.hpp"

namespace oips {
	class tar {
	private:
		std::string filename;

		tar(std::string filename)
			: filename(std::move(filename)) {}
	public:
		tar(const tar &) = default;
		tar(tar &&) = default;
		~tar() = default;

		result<std::vector<std::string>> list() const;
		result<std::string> read_file(std::string_view filename) const;
		result<std::monostate> extract_to(std::string_view dir) const;

		static result<tar> open(std::string filename);
	};
}

#endif // FILE_OIPS_TAR_HPP
