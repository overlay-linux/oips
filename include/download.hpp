#ifndef FILE_OIPS_DOWNLOAD_HPP
#define FILE_OIPS_DOWNLOAD_HPP
#include <string_view>

namespace oips {
	bool download(std::string_view url, std::string_view filename);
}

#endif // FILE_OIPS_DOWNLOAD_HPP
