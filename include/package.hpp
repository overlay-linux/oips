#ifndef FILE_OIPS_PACKAGE_HPP
#define FILE_OIPS_PACKAGE_HPP
#include <variant>
#include <vector>
#include <string>
#include "safmat.hpp"
#include "error.hpp"
#include "conf.hpp"

namespace oips {
	struct package_info {
		std::string name;
		std::string version;
		std::string description;
		std::string url;
		std::string target;
		std::string sha256;
		std::vector<std::string> licenses;
		std::vector<std::string> depends;
		std::vector<std::string> provides;
		std::vector<std::string> replaces;
		std::vector<std::string> conflicts;

		package_info(const package_info &) = default;
		package_info(package_info &&) = default;
		~package_info() = default;


		static result<package_info> parse(const conf::config &);
		static result<package_info> extract_from_oip(std::string oip);
	private:
		package_info() = default;
	};
}

template<>
struct safmat::Formatter<oips::package_info> : safmat::internal::PaddedFormatter {
	void format_to(FormatContext &ctx, const oips::package_info &info);
};

#endif // FILE_OIPS_PACKAGE_HPP
