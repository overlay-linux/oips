#ifndef FILE_OIPS_CMDLINE_HPP
#define FILE_OIPS_CMDLINE_HPP
#include <initializer_list>
#include <string_view>
#include <stdexcept>
#include <vector>
#include <string>
#include <array>

namespace oips::cmdline {
	struct operation;
	using args_list = std::vector<std::string_view>;

	namespace operations {
		extern operation *help;
		extern operation *info;
	}

	struct option {
		const std::vector<std::string_view> names;
		const std::string_view description;
		const bool takes_arg;
		std::string_view arg{};
		bool selected{ false };

		constexpr option(std::vector<std::string_view> names,
						 std::string_view desc,
						 bool takes_arg) noexcept
			: names(names), description(desc), takes_arg(takes_arg) {}

		constexpr explicit operator bool() const noexcept { return selected; }
	};

	struct operation {
		const std::string_view name;
		const std::string_view usage;
		const std::string_view description;
		std::vector<option> options;

		operation(std::string_view name,
				  std::string_view usage,
				  std::string_view desc,
				  const std::initializer_list<option> &opts = {})
			: name(name), usage(usage), description(desc), options(opts) {}

		virtual ~operation() = default;
		virtual int operator()(const args_list &args) = 0;
		option &operator[](std::string_view name);
		option *get(std::string_view name);

		static operation *global;
		static operation *get_op(std::string_view name);

		inline static const std::array operations = {
			operations::help,
			operations::info,
		};
	};

	int parse(int argc, char *argv[]);
} // namespace oips::cmdline

#endif // FILE_OIPS_CMDLINE_HPP
