#ifndef FILE_OIPS_ERROR_HPP
#define FILE_OIPS_ERROR_HPP
#include <source_location>
#include <string_view>
#include <variant>
#include "safmat.hpp"

namespace oips {
#if __cpp_lib_source_location
    template<class E>
    [[noreturn]]
    inline static void raise(std::string_view msg, std::source_location loc = std::source_location::current()) {
		throw E(safmat::format("{}: {}", loc, msg));
    }
#else
    template<class E>
    [[noreturn]]
    inline static void raise(std::string_view msg) {
	throw E(msg);
    }
#endif

	struct error {
		std::string msg;

		template<class T, class... Args>
		error(const T &fmt, Args &&... args) : msg(safmat::format(fmt, std::forward<Args>(args)...)) {}
		error(std::string msg) : msg(std::move(msg)) {}

		error(const error &) = default;
		error(error &&) = default;
		~error() = default;
	};

	struct invalid_result : std::exception {
		std::string msg;
		
		invalid_result(std::string msg) : msg(std::move(msg)) {}
		invalid_result(const invalid_result &) = default;
		invalid_result(invalid_result &&) = default;
		~invalid_result() = default;

		const char *what() const noexcept { return msg.c_str(); }
	};
	
	template<class T>
	class [[nodiscard]] result {
	private:
		std::variant<T, error> value;
	public:
		explicit result(T x) : value(std::move(x)) {}
		result(error e) : value(std::move(e)) {}
		result(const result &) = default;
		result(result &&) = default;
		~result() = default;

		explicit operator bool() const noexcept { return value.index() == 0; }
		const std::string &get_error() const { return std::get<1>(value).msg; }

#if __cpp_lib_source_location
		T &expect(std::string_view msg, std::source_location loc = std::source_location::current()) {
			if (*this) {
				return std::get<0>(value);
			} else {
				raise<invalid_result>(safmat::format("{}: {}", msg, std::get<1>(value).msg), loc);
			}
		}
		const T &expect(std::string_view msg, std::source_location loc = std::source_location::current()) const {
			if (*this) {
				return std::get<0>(value);
			} else {
				raise<invalid_result>(safmat::format("{}: {}", msg, std::get<1>(value).msg), loc);
			}
		}

		T &unwrap(std::source_location loc = std::source_location::current()) { return expect("Called unwrap() on an errorneous", loc); }
		const T &unwrap(std::source_location loc = std::source_location::current()) const { return expect("Called unwrap() on an errorneous", loc); }
#else
		T &expect(std::string_view msg) {
			if (*this) {
				return std::get<0>(value);
			} else {
				raise<invalid_result>(safmat::format("{}: {}", msg, std::get<1>(value).msg));
			}
		}
		const T &expect(std::string_view msg) const {
			if (*this) {
				return std::get<0>(value);
			} else {
				raise<invalid_result>(safmat::format("{}: {}", msg, std::get<1>(value).msg));
			}
		}

		T &unwrap() { return expect("Called unwrap() on an errorneous"); }
		const T &unwrap() const { return expect("Called unwrap() on an errorneous"); }
#endif
	};

	template<class T>
	inline result<T> ok(T x) { return result<T>(std::move(x)); }
	inline result<std::monostate> ok() { return result<std::monostate>(std::monostate()); }
}

#endif // FILE_OIPS_ERROR_HPP
