#ifndef FILE_OIPS_UTILS_HPP
#define FILE_OIPS_UTILS_HPP
#include <string>
#include <vector>
#include <cstdio>
#include "error.hpp"

namespace oips {
	std::string read(std::FILE *);
	bool read_line(std::FILE *, std::string &);
	std::vector<std::string> read_lines(std::FILE *);
}

#endif // FILE_OIPS_UTILS_HPP
